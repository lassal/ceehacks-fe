import { Component, OnInit } from '@angular/core';
import { MatSlideToggleChange } from '@angular/material';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-monitoring',
  templateUrl: './monitoring.component.html',
  styleUrls: ['./monitoring.component.scss']
})
export class MonitoringComponent {

  deviceMovedRecently = false;
  lastLog;
  interval;
  notifyResponse;
  notifyLvl;

  constructor(private http: HttpClient) {
  }

  public change(event: MatSlideToggleChange) {
    clearInterval(this.interval);
    this.deviceMovedRecently = false;
    this.notifyLvl = null;

    console.log(event);
    if (event.checked) {
      this.startWatching();
    }
  }

  private startWatching() {
    this.interval = setInterval(() => {
      this.getDeviceLog();
    }, 100);
  }

  private getDeviceLog() {
    this.http.get('http://localhost:4200/getLogs').subscribe((log) => {
      // console.log('getDeviceLog:', log);
      const userLog = log[8266];
      if (this.lastLog && Array.isArray(this.lastLog) && Array.isArray(userLog)) {
        if (this.lastLog.length < userLog.length) {
          this.deviceMovedRecently = true;
        }
      }
      this.lastLog = userLog;
    });
  }

  notifyPatient() {
    this.http.get('http://localhost:4200/notifyPatients').subscribe((response: any) => {
      console.log('notifyPatient:', response);
      this.notifyLvl = response.notify_level;
      // this.notifyResponse = response;
    });
  }
}

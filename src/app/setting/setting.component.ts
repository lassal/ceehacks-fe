import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit {
  patientLoaded = false;
  patient = {
    name: 'Miroslav',
    lastname: 'Novak',
    patient_phonenumber: '+420605142060',
    buddy_phonenumber: '+420601563706',
    medicine: [{name: 'Medformin 500 MG', dosage: [1, 1, 1, 0]}, {name: 'Jardiance', dosage: [1, 0, 0, 0]}]
  };

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  constructor(private formBuilder: FormBuilder, private http: HttpClient) {
  }

  ngOnInit() {
    this.firstFormGroup = this.formBuilder.group({
      id: ['', Validators.required]
    });
    this.secondFormGroup = this.formBuilder.group({
      name: [this.patient.name, Validators.required],
      lastname: [this.patient.lastname, Validators.required],
      patient_phonenumber: [this.patient.patient_phonenumber, Validators.required],
      buddy_phonenumber: [this.patient.buddy_phonenumber, Validators.required],
    });
  }

  loadPatientData() {
    setTimeout(() => {
      this.patientLoaded = true;
    }, 400);
  }

}
